import yaml
import csv

yaml_file = "reddit.yaml"

# -----------------------------------------------------------------------------

with open(yaml_file, 'r') as stream:
    infile = yaml.load(stream)

rows = []
rows.append(['subreddit', 'postname', 'link', 'poststatus'])

for key in infile:
    for title in infile[key]:
        sub    = key
        link   = infile[key][title][0]
        status = infile[key][title][1]
        rows.append([sub, title, link, status])

with open('reddit.csv', 'w', newline='') as stream:
    outfile = csv.writer(stream)
    outfile.writerows(rows)
