# YAML of all Stared Reddit Posts
This Python script ("reddit.py") gathers information on stared reddit posts and lists them in a YAML-file.
There is a Python script ("to_csv.py") which reads the YAML-file and outputs a CSV-file instead.
