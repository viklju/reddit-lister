# This doesn't work perfeclty and is hastly cobbled together

import praw
import yaml

 # Useful documentation:
 # https://praw.readthedocs.io/en/latest/code_overview/models/submission.html
 # https://github.com/praw-dev/praw
 # https://pythonprogramming.net/introduction-python-reddit-api-wrapper-praw-tutorial/
 # https://www.reddit.com/r/redditdev/comments/44a7xm/praw_how_to_tell_if_a_submission_has_been_removed/
 # https://pyyaml.org/wiki/PyYAMLDocumentation

def check_removed(post):
    if post.author is None:
        if post.selftext == '[deleted]':
            return "post deleted"
        if post.selftext == '[removed]':
            return "post removed"
        return "account deleted"
    if post.selftext == '[removed]':
        return "post removed"
    else:
        return "post open"


reddit = praw.Reddit(client_id='', client_secret="",
                     password='', user_agent='praw',
                     username='')
# Needs to create an "app" --> https://old.reddit.com/prefs/apps/

user = reddit.redditor(name='')

output = {}

for post in user.saved():  # user.saved(limit=5) for testing
    if post.subreddit.display_name not in list(output.keys()):
        output[post.subreddit.display_name] = {post.title: ['https://reddit.com' + post.permalink, check_removed(post)]}
    else:
        output[post.subreddit.display_name].update({post.title: ['https://reddit.com' + post.permalink, check_removed(post)]})

# Still requires some manuall formatting (some titles break it)
with open(r'E:\nextcloud\Programming\reddit.yaml', 'w') as file:
    write = yaml.dump(output, file, default_flow_style=False)
